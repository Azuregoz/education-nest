import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FileModule } from './file/file.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from "path";
import {path} from 'app-root-path';
import { FotocartochkaModule } from './fotocartochka/fotocartochka.module';
import { Fotocartochka } from './fotocartochka/entities/fotocartochka.entity';
import { TestModule } from './test/test.module';
require('dotenv').config()

@Module({
  imports: [TypeOrmModule.forRoot(
    {
      type: process.env.DB_TYPE,
      host: process.env.DB_HOST,
      port: process.env.DB_PORT,
      username: process.env.DB_USER,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [Fotocartochka],
      subscribers: [],
      synchronize: true,
      keepConnectionAlive: true,
      migrationsRun: false,
    }
  ),

  ServeStaticModule.forRoot({
    rootPath: join(path, 'static'),
  }),
  
   FileModule,
   FotocartochkaModule,
   TestModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
