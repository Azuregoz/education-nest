import { HttpStatus, Injectable } from '@nestjs/common';
import * as pathV2 from 'path';
import * as fs from 'fs';
import * as uuid from 'uuid';
import {path} from 'app-root-path';

@Injectable()
export class FileService {

    async createFile (file: any){
        try {
            const type = await file.originalname.split('.');
            const filename = await uuid.v4() + '.' + type[1];
            const filePath = await pathV2.resolve(path,'static');
            if (!fs.existsSync(filePath)){
                fs.mkdirSync(filePath, {recursive: true})
            }
            fs.writeFileSync(pathV2.join(filePath, filename), file.buffer)
            return filename
        }
        catch (e){
            throw HttpStatus.INTERNAL_SERVER_ERROR
        }
    }
}
