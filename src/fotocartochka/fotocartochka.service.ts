import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FileService } from 'src/file/file.service';
import { Repository } from 'typeorm';
import { CreateFotocartochkaDto } from './dto/create-fotocartochka.dto';
import { Fotocartochka } from './entities/fotocartochka.entity';

@Injectable()
export class FotocartochkaService {
  constructor(
    @InjectRepository(Fotocartochka)
    private readonly repository: Repository<Fotocartochka>,
    private readonly fileService: FileService
  ){}

  async create(createFotocartochkaDto: CreateFotocartochkaDto , image:any) {
    const photo = await this.fileService.createFile(image)
    return this.repository.save({
      name: createFotocartochkaDto.name,
      image: photo
    });
  }

  async findAll() {
    let data = await this.repository.find();
    
    for (let i = 0; i < data.length; i++) {
      if (data[i].image !== null) {
        data[i].image = `http://127.0.0.1:3000/${data[i].image}`
      }
    }
    return data
  }

  async findOne(id: number) {
    let data = await this.repository.find({where:{id:id}});
    for (let i = 0; i < data.length; i++) {
      if (data[i].image !== null) {
        data[i].image = `http://127.0.0.1:3000/${data[i].image}`
      }
    }
    return data
  }
}
