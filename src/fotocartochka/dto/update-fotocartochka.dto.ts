import { PartialType } from '@nestjs/mapped-types';
import { CreateFotocartochkaDto } from './create-fotocartochka.dto';

export class UpdateFotocartochkaDto extends PartialType(CreateFotocartochkaDto) {}
