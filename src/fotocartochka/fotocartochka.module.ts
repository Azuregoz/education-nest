import { Module } from '@nestjs/common';
import { FotocartochkaService } from './fotocartochka.service';
import { FotocartochkaController } from './fotocartochka.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Fotocartochka } from './entities/fotocartochka.entity';
import { FileModule } from 'src/file/file.module';

@Module({
  imports: [TypeOrmModule.forFeature([Fotocartochka]), FileModule],
  controllers: [FotocartochkaController],
  providers: [FotocartochkaService]
})
export class FotocartochkaModule {}
