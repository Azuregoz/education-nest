import { Controller, Get, Post, Body, Patch, Param, Delete, UseInterceptors, UploadedFile } from '@nestjs/common';
import { FotocartochkaService } from './fotocartochka.service';
import { CreateFotocartochkaDto } from './dto/create-fotocartochka.dto';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('fotocartochka')
export class FotocartochkaController {
  constructor(private readonly fotocartochkaService: FotocartochkaService) {}

  @Post()
  @UseInterceptors(FileInterceptor('image'))
  create(@Body() createFotocartochkaDto: CreateFotocartochkaDto, @UploadedFile() image) {
    return this.fotocartochkaService.create(createFotocartochkaDto,image);
  }

  @Get()
  findAll() {
    return this.fotocartochkaService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.fotocartochkaService.findOne(+id);
  }
}
