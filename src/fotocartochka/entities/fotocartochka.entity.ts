import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Fotocartochka {
    @PrimaryGeneratedColumn()
    id:number;

    @Column()
    name:string;

    @Column()
    image:string;
}
